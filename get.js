var DHT = require('bittorrent-dht');
var ed = require('ed25519-supercop');
var dht = new DHT({verify: ed.verify});

dht.listen(function() {
    console.log('dht is listening:');
});

dht.on('ready', function () {
    console.log('dht is ready');
    dht.get('509dd518feaa11caadb079972a84c2c850f613a4', function (err, res) {
        if (err) {
            console.error('failed to get reply:', err);
        } else {
            console.log('get reply:', res.v.toString());
        }
    });
});
