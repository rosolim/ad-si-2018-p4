var ed = require('ed25519-supercop');
var keypair = ed.createKeyPair(ed.createSeed());

var value = new Buffer(200).fill('teste'); // the payload you want to send
var opts = {
    k: keypair.publicKey,
    seq: 0,
    v: value,
    sign: function (buf) {
        return ed.sign(buf, keypair.publicKey, keypair.secretKey)
    }
};

var DHT = require('bittorrent-dht');
var dht = new DHT();

dht.listen(function () {
    console.log('dht is listening');
});

dht.on('ready', function() {
    console.log('dht is ready');
    dht.put(opts, function (err, hash, nodesAccepted) {
        console.error('error=', err);
        console.log('hash=', hash.toString('hex'), 'nodes accepted:', nodesAccepted);
    })
});
